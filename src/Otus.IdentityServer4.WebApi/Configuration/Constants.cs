﻿namespace Otus.IdentityServer4.WebApi.Configuration
{
    internal static class Constants
    {
        public const string AuthPolicyName = "ApiScope";
        public const string ScopeClaimName = "scope";
        public static readonly string[] AllowedScopeValues = { "scope1" };
    }
}
