using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.IdentityServer4.WorkerService.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.IdentityServer4.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly IForecastService _forecastService;
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger, IForecastService forecastService)
        {
            _forecastService = forecastService;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
            var forecast = await _forecastService.GetForecast();
            if (forecast.HasError)
            {
                _logger.LogError(forecast.ErrorMessage);
            }
            else
            {
                _logger.LogInformation(forecast.Response);
            }            
        }
    }
}
