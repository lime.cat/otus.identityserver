﻿using Otus.IdentityServer4.WorkerService.Models;
using System.Threading.Tasks;

namespace Otus.IdentityServer4.WorkerService.Services
{
    public interface IForecastService
    {
        Task<ForecastApiResponse> GetForecast();
    }
}
