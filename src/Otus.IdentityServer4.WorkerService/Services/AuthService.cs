﻿using IdentityModel.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Otus.IdentityServer4.WorkerService.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.IdentityServer4.WorkerService.Services
{
    public class AuthService : IAuthService
    {
        private readonly AuthOptions _authOptions;
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public AuthService(IHttpClientFactory httpClientFactory, IOptions<AuthOptions> options, ILogger<AuthService> logger)
        {
            _authOptions = options.Value;
            _httpClient = httpClientFactory.CreateClient(HttpClients.AuthorityClient);
            _logger = logger;
        }

        public async Task<TokenResponse> Authorize()
        {
            var doc = await _httpClient.GetDiscoveryDocumentAsync(_authOptions.Authority);
            if (doc.IsError)
            {
                return null;
            }
            var token = await _httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = doc.TokenEndpoint,
                ClientId = _authOptions.ClientId,
                ClientSecret = _authOptions.ClientSecret,
                Scope = _authOptions.Scope
            });
            _logger.LogInformation($"Access token: {token.AccessToken}");
            return token;
        }
    }
}
