﻿using IdentityModel.Client;
using Otus.IdentityServer4.WorkerService.Configuration;
using Otus.IdentityServer4.WorkerService.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.IdentityServer4.WorkerService.Services
{
    public class ForecastService : IForecastService
    {
        private readonly IAuthService _authService;
        private readonly IHttpClientFactory _httpClientFactory;

        public ForecastService(IHttpClientFactory httpClientFactory, IAuthService authService)
        {
            _authService = authService;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<ForecastApiResponse> GetForecast()
        {
            var tokenResponse = await _authService.Authorize();
            if (tokenResponse?.IsError != false)
            {
                return new ForecastApiResponse { ErrorMessage = "Authorization failed" };
            }
            
            var apiClient = _httpClientFactory.CreateClient(HttpClients.ForecastClient);
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync("WeatherForecast");
            if(!response.IsSuccessStatusCode)
            {
                return new ForecastApiResponse { ErrorMessage = $"Not successful response. Reason: {response.ReasonPhrase}" };
            }
            var content = await response.Content.ReadAsStringAsync();
            return new ForecastApiResponse { Response = content };
        }
    }
}
