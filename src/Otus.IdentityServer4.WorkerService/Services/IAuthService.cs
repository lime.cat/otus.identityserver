﻿using IdentityModel.Client;
using System.Threading.Tasks;

namespace Otus.IdentityServer4.WorkerService.Services
{
    public interface IAuthService
    {
        Task<TokenResponse> Authorize();
    }
}
