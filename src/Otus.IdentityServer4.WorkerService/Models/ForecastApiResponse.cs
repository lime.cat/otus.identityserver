﻿namespace Otus.IdentityServer4.WorkerService.Models
{
    public class ForecastApiResponse
    {
        public string Response { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError => !string.IsNullOrEmpty(ErrorMessage);
    }
}
