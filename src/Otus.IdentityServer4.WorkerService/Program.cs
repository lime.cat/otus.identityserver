using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.IdentityServer4.WorkerService.Configuration;
using Otus.IdentityServer4.WorkerService.Services;

namespace Otus.IdentityServer4.WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.Configure<AuthOptions>(hostContext.Configuration.GetSection(nameof(AuthOptions)));
                    services.AddTransient<IAuthService, AuthService>();
                    services.AddTransient<IForecastService, ForecastService>();
                    services.ConfigureHttpClients(hostContext.Configuration, hostContext.HostingEnvironment);
                });
    }
}
