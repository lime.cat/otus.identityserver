﻿namespace Otus.IdentityServer4.WorkerService.Configuration
{
    public class AuthOptions
    {
        public string Authority { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scope { get; set; }
    }
}
