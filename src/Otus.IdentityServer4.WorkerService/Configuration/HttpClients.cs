﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Net.Http;

namespace Otus.IdentityServer4.WorkerService.Configuration
{
    public static class HttpClients
    {
        public const string AuthorityClient = "AuthorityHttpClient";
        public const string ForecastClient = "ForecastHttpClient";

        public static IServiceCollection ConfigureHttpClients(this IServiceCollection services, IConfiguration configuration, IHostEnvironment environment)
        {
            var authClient = services.AddHttpClient(AuthorityClient, client =>
            {
                client.BaseAddress = configuration.GetValue<Uri>("AuthOptions:Authority");
            });

            var apiClient = services.AddHttpClient(ForecastClient, client =>
            {
                client.BaseAddress = configuration.GetValue<Uri>("ClientOptions:ForecastApiUrl");
            });

            if (environment.IsDevelopment())
            {
                var messageHandler = new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, cert, chain, error) => true
                };

                authClient.ConfigurePrimaryHttpMessageHandler(_ => messageHandler);
                apiClient.ConfigurePrimaryHttpMessageHandler(_ => messageHandler);
            }
            return services;
        }
    }
}
